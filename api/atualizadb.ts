import dotenv from 'dotenv';
dotenv.config();

import { Sequelize } from 'sequelize';
import db from './db';
import { User } from './models/User';

async function atualizaDb()
{
    await User.sync({force:true});
    await User.create({
        name:"Mariana",
        birthdate: '12-07-1994',
        cpf: '00000000011',
        address: 'Rua Teste',
        number: 123,
        zipcode: '12345000',
        phone: '51999990000',
        email:"mari@crie.ti",
        password:"9999",
    });

    let logado : any = await User.locateUser('mari@crie.ti','9999');
    console.log(logado.toJSON());
}

atualizaDb();
import { Op } from 'sequelize';
import { Airplane } from '../models/Airplane';
import { Logs } from '../models/Logs';

import express, { Express, Request, Response, NextFunction } from 'express';

class AirplaneController {

  index = async (req: Request, res: Response, next: NextFunction) => {

    const airplane = await Airplane.findAll({});
    res.json(airplane);
  }

  create = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const data = await this._validateData(req.body);
      const airplane = await Airplane.create(data);

      Logs.create({
        action: `Airplane ${data.model} created`
      });

      res.json(airplane);
    } catch (error : any) {
      res.status(400).json({ error: error.message });
    }
  }

  show = async (req: Request, res: Response, next: NextFunction) => {
    const airplane = await Airplane.findByPk(req.params.cityId);
    res.json(airplane);
  }

  update = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const id = req.params.airplaneId;
      const data = await this._validateData(req.body);
      await Airplane.update(data, {
        where: {
          id: id
        }
      });

      Logs.create({
        action: `Airplane ID ${id} updated`
      });

      res.json(await Airplane.findByPk(id));
    } catch (error : any) {
      res.status(400).json({ error: error.message });
    }
  }

  delete = async (req: Request, res: Response, next: NextFunction) => {
   
    const id = req.params.airplaneId;

    await Airplane.destroy({
      where: {
        id: req.params.airplaneId
      }
    });

    Logs.create({
      action: `Airplane ID ${id} deleted`
    });

    res.json({});
  }

  _validateData = async (data: any) => {
    const attributes : any = ['model', 'seats'];
    const airplane : any = { };
    
    for (const attribute of attributes) {
      if (!data[attribute]) {
        throw new Error(`The attribute "${attribute}" is required.`);
      }
      airplane[attribute] = data[attribute];
    }

    return airplane;
  }

}

export default new AirplaneController();

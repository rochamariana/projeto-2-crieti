import { Op } from 'sequelize';
import { Airplane } from '../models/Airplane';
import { Flight } from '../models/Flight';
import { Logs } from '../models/Logs';
import express, { Express, Request, Response, NextFunction } from 'express';

class FlightController {

  index = async (req: Request, res: Response, next: NextFunction) => {

    const params = req.query;
    const limit : number = params.page ? parseInt(params.limit as string) : 100;
    const page : number = params.page ? parseInt(params.page as string) : 1;
    const offset : number = (page - 1) * limit;
    const sort : string = params.sort ? params.sotr as string : 'id';
    const order : string = params.order ? params.order as string : 'ASC';
    const where : any = {};

    if (params.AirplaneId) {
      where.AirplaneId = 
      {
        [Op.eq]: params.AirplaneId
      };
    }

    const flight = await Flight.findAll({
      where: where,
      include: [{
        model: Airplane,
        required: false,
        attributes: ['model', 'seats']
      }]
    });

    res.json(flight);
  }

  create = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const data = await this._validateData(req.body);
      const flight = await Flight.create(data);

      Logs.create({
        action: `Flight number ${data.number} created`
      });

      res.json(flight);
    } catch (error : any) {
      res.status(400).json({ error: error.message });
    }
  }

  show = async (req: Request, res: Response, next: NextFunction) => {
    const flight = await Flight.findByPk(req.params.cityId);
    res.json(flight);
  }

  update = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const id = req.params.flightId;
      const data = await this._validateData(req.body);
      
      await Flight.update(data, {
        where: {
          id: id
        }
      });

      Logs.create({
        action: `Flight ID ${id} updated`
      });

      res.json(await Flight.findByPk(id));
    } catch (error : any) {
      res.status(400).json({ error: error.message });
    }
  }

  delete = async (req: Request, res: Response, next: NextFunction) => {

    const id = req.params.flightId;

    await Flight.destroy({
      where: {
        id: req.params.flightId
      }
    });

    Logs.create({
      action: `Flight ID ${id} deleted`
    });

    res.json({});
  }

  _validateData = async (data : any, id? : any) => {
    const attributes : any = ['number', 'connection' , 'baggage', 'departure_datetime', 'arrival_datetime', 'ticket_price', 'ticket_tax', 'departure_airport', 'arrival_airport', 'AirplaneId', 'AirlineId'];
    const flight : any = { };
    
    for (const attribute of attributes) {
      if (!data[attribute]) {
        throw new Error(`The attribute "${attribute}" is required.`);
      }
      flight[attribute] = data[attribute];
    }

    return flight;
  }

}

export default new FlightController();

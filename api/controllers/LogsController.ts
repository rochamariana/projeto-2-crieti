import LogModel from '../models/Logs';
import express, { Express, Request, Response, NextFunction } from 'express';

class LogsController {

  create = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const log = await LogModel.create(req.body);
      res.json(log);
    } catch (error) {
      res.status(400).json({ error: error.message });
    }
  }

}

export default new LogsController();
import { Op } from 'sequelize';
import { City } from '../models/City';
import { State } from '../models/State';
import { Logs } from '../models/Logs';

import express, { Express, Request, Response, NextFunction } from 'express';

class CitiesController {

  index = async (req: Request, res: Response, next: NextFunction) => {

    const params = req.query;
    const limit : number = params.page ? parseInt(params.limit as string) : 100;
    const page : number = params.page ? parseInt(params.page as string) : 1;
    const offset : number = (page - 1) * limit;
    const sort : string = params.sort ? params.sotr as string : 'id';
    const order : string = params.order ? params.order as string : 'ASC';
    const where : any = {};

    if (params.StateId) {
      where.StateId = 
      {
        [Op.eq]: params.StateId
      };
    }

    const cities = await City.findAll({
      where: where,
      include: [{
        model: State,
        required: false,
        attributes: ['name', 'province']
      }]
    });

    res.json(cities);
  }

  create = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const data = await this._validateData(req.body);
      const city = await City.create(data);

      Logs.create({
        action: `City ${data.name} created`
      });

      res.json(city);
    } catch (error : any) {
      res.status(400).json({ error: error.message });
    }
  }

  show = async (req: Request, res: Response, next: NextFunction) => {
    const state = await City.findByPk(req.params.cityId);
    res.json(state);
  }

  update = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const id = req.params.cityId;
      const data = await this._validateData(req.body);
      await City.update(data, {
        where: {
          id: id
        }
      });

      Logs.create({
        action: `City ID ${id} updated`
      });

      res.json(await City.findByPk(id));
    } catch (error : any) {
      res.status(400).json({ error: error.message });
    }
  }

  delete = async (req: Request, res: Response, next: NextFunction) => {
    const id = req.params.cityId;

    await City.destroy({
      where: {
        id: req.params.cityId
      }
    });

    Logs.create({
      action: `City ID ${id} deleted`
    });

    res.json({});
  }

  _validateData = async (data: any) => {
    const attributes : any = ['name', 'StateId'];
    const city : any = { };
    
    for (const attribute of attributes) {
      if (!data[attribute]) {
        throw new Error(`The attribute "${attribute}" is required.`);
      }
      city[attribute] = data[attribute];
    }

    if (await this._checkIfCityAndStateExists(city.name, city.StateId)) {
      throw new Error(`The city in the State "${city.StateId}" already exists.`);
    }

    return city;
  }

  _checkIfCityAndStateExists = async (name: string, state: string) => {
    const cities = await City.count({
      where: {
        [Op.and]: [
          { StateId: state },
          { name: name }
        ]
      }
    });

    return cities > 0;
  }

}

export default new CitiesController();


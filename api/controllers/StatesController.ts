import { Op } from 'sequelize';
import { State } from '../models/State';
import { Logs } from '../models/Logs';

import express, { Express, Request, Response, NextFunction} from 'express';

class StatesController {

  index = async (req : Request, res : Response, next : NextFunction) => {
    const states = await State.findAll({});
    res.json(states);
  }

  create = async (req : Request, res : Response, next : NextFunction) => {
    try {
      const data = await this._validateData(req.body);
      const state = await State.create(data);

      Logs.create({
        action: `State ${data.province} created`
      });

      res.json(state);
    } catch (error : any) {
      res.status(400).json({ error: error.message });
    }
  }

  show = async (req : Request, res : Response, next : NextFunction) => {
    const state = await State.findByPk(req.params.stateId);
    res.json(state);
  }

  update = async (req : Request, res : Response, next : NextFunction) => {
    try {
      const id : any = req.params.stateId;
      const data = await this._validateData(req.body, id);
      await State.update(data, {
        where: {
          id: id
        }
      });

      Logs.create({
        action: `State ID ${id} updated`
      });

      res.json(await State.findByPk(id));
    } catch (error : any) {
      res.status(400).json({ error: error.message });
    }
  }

  delete = async (req : Request, res : Response, next : NextFunction) => {

    const id : any = req.params.stateId;

    await State.destroy({
      where: {
        id: req.params.stateId
      }
    });

    Logs.create({
      action: `State ID ${id} deleted`
    });

    res.json({});
  }

  _validateData = async (data : any, id? : any) => {
    const attributes : any = ['name', 'province'];
    const state : any = {};
    
    for (const attribute of attributes) {
      if (!data[attribute]) {
        throw new Error(`The attribute "${attribute}" is required.`);
      }
      state[attribute] = data[attribute];
    }

    return state;
  }

}

export default new StatesController();

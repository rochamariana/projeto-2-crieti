import { Op } from 'sequelize';
import { User } from '../models/User';
import { City } from '../models/City';
import { Logs } from '../models/Logs';

import express, { Express, Request, Response, NextFunction} from 'express';

class UsersController {

  index = async (req : Request, res : Response, next : NextFunction) => {
    const params = req.query;
    const limit : number = params.page ? parseInt(params.limit as string) : 100;
    const page : number = params.page ? parseInt(params.page as string) : 1;
    const offset : number = (page - 1) * limit;
    const sort : string = params.sort ? params.sotr as string : 'id';
    const order : string = params.order ? params.order as string : 'ASC';
    const where : any = {};

    if (params.name) {
      where.name = {
        [Op.iLike]: `%${params.name}%`
      };
    }

    if (params.email) {
      where.email = {
        [Op.iLike]: `%${params.email}%`
      };
    }

    if (params.min_age) {
      where.age = {
        [Op.gte]: params.min_age
      };
    }

    if (params.max_age) {
      if (! where.age) {
        where.age = {};
      }
      where.age[Op.lte] = params.max_age;
    }

    if (params.sex) {
      where.sex = params.sex;
    }

    const users = await User.findAll({
      where: where,
      limit: limit,
      offset: offset,
      order: [[sort, order]],
      include: [{
        model: City,
        required: false,
        attributes: ['name', 'StateId']
      }]
    });
    
    res.json(users);
  }

  create = async (req : Request, res : Response, next : NextFunction) => {
    try {
      const data = await this._validateData(req.body);
      const user = await User.create(data);

      Logs.create({
        action: `User name ${data.name} created`
      });

      res.json(user);
    } catch (error : any) {
      res.status(400).json({ error: error.message });
    }
  }

  show = async (req : Request, res : Response, next : NextFunction) => {
    const user = await User.findByPk(req.params.userId);
    res.json(user);
  }

  update = async (req : Request, res : Response, next : NextFunction) => {
    try {
      const id = req.params.userId;
      const data = await this._validateData(req.body, id);
      await User.update(data, {
        where: {
          id: id
        }
      });

      Logs.create({
        action: `User ID ${id} updated`
      });

      res.json(await User.findByPk(id));
    } catch (error : any) {
      res.status(400).json({ error: error.message });
    }
  }

  delete = async (req : Request, res : Response, next : NextFunction) => {
    
    const id = req.params.userId;

    await User.destroy({
      where: {
        id: req.params.userId
      }
    });

    Logs.create({
      action: `User ID ${id} deleted`
    });

    res.json({});
  }

  _validateData = async (data : any, id? : any) => {
    const attributes : any = ['name', 'cpf', 'birthdate', 'address', 'zipcode' ,'phone', 'email', 'password', 'CityId'];
    const user : any = {};

    for (const attribute of attributes) {
      if (! data[attribute]){
        throw new Error(`The attribute "${attribute}" is required.`);
      }
      user[attribute] = data[attribute];
    }

    // if (await this._checkIfEmailExists(user.email, id)) {
    //   throw new Error(`The user with mail address "${user.email}" already exists.`);
    // }

    return user;
  }

  _checkIfEmailExists = async (email : any , id? : any) => {
    const where : any = {
      email: email,
      id : id
    };

    if (id) {
      where.id = { [Op.ne]: id }; // WHERE id != id
    }

    const count = await User.count({
      where: where
    });

    return count > 0;
  }

}

export default new UsersController();

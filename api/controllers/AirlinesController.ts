import { Op } from 'sequelize';
// import { City } from '../models/City';
import { Airline } from '../models/Airline';
import { Logs } from '../models/Logs';

import express, { Express, Request, Response, NextFunction } from 'express';

class AirlineController {

  index = async (req: Request, res: Response, next: NextFunction) => {

    const params = req.query;
    const limit : number = params.page ? parseInt(params.limit as string) : 100;
    const page : number = params.page ? parseInt(params.page as string) : 1;
    const offset : number = (page - 1) * limit;
    const sort : string = params.sort ? params.sotr as string : 'id';
    const order : string = params.order ? params.order as string : 'ASC';
    const where : any = {};

    if (params.CityId) {
      where.CityId = 
      {
        [Op.eq]: params.CityId
      };
    }

    const airlines = await Airline.findAll({
      where: where
    });

    res.json(airlines);
  }

  create = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const data = await this._validateData(req.body);
      const airline = await Airline.create(data);

      Logs.create({
        action: `Airline ${data.name} created`
      });

      res.json(airline);
    } catch (error : any) {
      res.status(400).json({ error: error.message });
    }
  }

  show = async (req: Request, res: Response, next: NextFunction) => {
    const airline = await Airline.findByPk(req.params.cityId);
    res.json(airline);
  }

  update = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const id = req.params.airlineId;
      const data = await this._validateData(req.body);
      await Airline.update(data, {
        where: {
          id: id
        }
      });

      Logs.create({
        action: `Airline ID ${id} updated`
      });

      res.json(await Airline.findByPk(id));
    } catch (error : any) {
      res.status(400).json({ error: error.message });
    }
  }

  delete = async (req: Request, res: Response, next: NextFunction) => {
    const id = req.params.airlineId;

    await Airline.destroy({
      where: {
        id: req.params.airlineId
      }
    });

    Logs.create({
      action: `Airline ID ${id} deleted`
    });

    res.json({});
  }

  _validateData = async (data: any) => {
    const attributes : any = ['name', 'iata_code'];
    const airline : any = { };
    
    for (const attribute of attributes) {
      if (!data[attribute]) {
        throw new Error(`The attribute "${attribute}" is required.`);
      }
      airline[attribute] = data[attribute];
    }

    return airline;
  }

}

export default new AirlineController();

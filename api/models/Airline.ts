import { DataTypes, Model } from 'sequelize';
import db from '../db';

export class Airline extends Model { };

Airline.init({
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    allowNull: false,
    primaryKey: true
  },
  name: {
    type: DataTypes.STRING,
    allowNull: false
  },
  iata_code:{
    type: DataTypes.STRING(2),
    allowNull: false
  }
}, {
  sequelize: db,
  tableName: 'airlines',
  modelName: 'Airline'
});


export default Airline;
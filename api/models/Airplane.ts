import { DataTypes, Model } from 'sequelize';
import db from '../db';

export class Airplane extends Model { };

Airplane.init({
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    allowNull: false,
    primaryKey: true
  },
  model: {
    type: DataTypes.STRING,
    allowNull: false
  },
  seats:{
    type: DataTypes.INTEGER,
    allowNull: false
  }
}, {
  sequelize: db,
  tableName: 'airplanes',
  modelName: 'Airplane'
});

export default Airplane;
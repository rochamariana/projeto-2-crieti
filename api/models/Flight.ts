import { DataTypes, Model } from 'sequelize';
import db from '../db';
import { Airplane } from './Airplane';
import { Airline } from './Airline';

export class Flight extends Model { };

Flight.init({
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    allowNull: false,
    primaryKey: true
  },
  number: {
    type: DataTypes.STRING(6),
    allowNull: false
  },
  connection: {
    type: DataTypes.INTEGER,
    allowNull: false
  },
  baggage: {
    type: DataTypes.STRING,
    allowNull: false
  },
  departure_datetime: {
    type: DataTypes.DATE,
    allowNull: false
  },
  arrival_datetime: {
    type: DataTypes.DATE,
    allowNull: false
  },
  ticket_price: {
    type: DataTypes.STRING,
    allowNull: false
  },
  ticket_tax: {
    type: DataTypes.STRING,
    allowNull: false
  },
  departure_airport: {
    type: DataTypes.STRING,
    allowNull: false
  },
  arrival_airport: {
    type: DataTypes.STRING,
    allowNull: false
  }
}, {
  sequelize: db,
  tableName: 'flights',
  modelName: 'Flight'
});


Airplane.hasMany(Flight);
Flight.belongsTo(Airplane);

Airline.hasMany(Flight);
Flight.belongsTo(Airline);

export default Flight;
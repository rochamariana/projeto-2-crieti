import { DataTypes, Model } from 'sequelize';
import db from '../db';
import City from './City';

export class User extends Model
{ 
  static async locateUser(email : any , password : any)
  {
    return await User.findOne({
      where: {
        email: email,
        password: password
      }
    });
  }
};

User.init({
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    allowNull: false,
    primaryKey: true
  },
  cpf:{
    type: DataTypes.STRING(14),
    allowNull: false
  },
  name: {
    type: DataTypes.STRING,
    allowNull: false
  },
  birthdate: {
    type: DataTypes.DATEONLY,
    allowNull: false
  },
  address: {
    type: DataTypes.STRING,
    allowNull: false
  },
  number: {
    type: DataTypes.STRING,
    allowNull: true
  },
  zipcode: {
    type: DataTypes.STRING(9),
    allowNull: false
  },
  phone: {
    type: DataTypes.STRING(14),
    allowNull: false
  },
  email: {
    type: DataTypes.STRING,
    allowNull: false
  },
  password: {
    type: DataTypes.STRING,
    allowNull: false
  }
}, {
  sequelize: db,
  tableName: 'users',
  modelName: 'User'
});

City.hasMany(User);
User.belongsTo(City);

export default User;
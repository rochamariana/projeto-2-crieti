import express, { Request, Response, NextFunction } from 'express';
import FlightsController from '../controllers/FlightsController';
import { Flight } from '../models/Flight';

const routerFlight = express.Router();

const validateFlightId = async (req: Request, res: Response, next: NextFunction) => {
  const flight = await Flight.findByPk(req.params.flightId);
  if (!flight) {
    return res.status(404).json({ error: 'Flight not found' });
  }
  next();
}

routerFlight.get('/flights', FlightsController.index);

routerFlight.post('/flights', FlightsController.create);

routerFlight.get('/flights/:flightId', validateFlightId, FlightsController.show);

routerFlight.put('/flights/:flightId', validateFlightId, FlightsController.update);

routerFlight.delete('/flights/:flightId', validateFlightId, FlightsController.delete);

export default routerFlight;
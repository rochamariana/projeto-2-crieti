import cors from 'cors';
import express, { } from 'express';
const router = express.Router();

import routerCity from './cities';
import routerUser from './users';
import routerLogin from './login';
import routerState from './states';
import routerAirplane from './airplanes';
import routerAirline from './airlines';
import routerFlight from './flights';

router.use(cors());

router.use(routerLogin);
router.use(routerUser);
router.use(routerState);
router.use(routerCity);
router.use(routerAirplane);
router.use(routerAirline);
router.use(routerFlight);

export default router;



import express, { Request, Response, NextFunction } from 'express';
import AirplanesController from '../controllers/AirplanesController';
import { Airplane } from '../models/Airplane';

const routerAirplane = express.Router();

const validateAirplaneId = async (req: Request, res: Response, next: NextFunction) => {
    const city = await Airplane.findByPk(req.params.airplaneId);
    if (!city) {
        return res.status(404).json({ error: 'Airplane not found' });
    }
    next();
}

routerAirplane.post('/airplanes', AirplanesController.create);

routerAirplane.get('/airplanes', AirplanesController.index);

routerAirplane.get('/airplanes/:airplaneId', validateAirplaneId, AirplanesController.show);

routerAirplane.put('/airplanes/:airplaneId', validateAirplaneId, AirplanesController.update);

routerAirplane.delete('/airplanes/:airplaneId', validateAirplaneId, AirplanesController.delete);

export default routerAirplane;
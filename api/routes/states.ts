import express, { Request, Response, NextFunction } from 'express';
import StatesController from '../controllers/StatesController';
import { State } from '../models/State';

const routerState = express.Router();

const validateStateId = async (req: Request, res: Response, next: NextFunction) => {
    const state = await State.findByPk(req.params.stateId);
    if (!state) {
        return res.status(404).json({ error: 'State not found' });
    }
    next();
}

routerState.post('/states', StatesController.create);

routerState.get('/states', StatesController.index);

routerState.get('/states/:stateId', validateStateId, StatesController.show);

routerState.put('/states/:stateId', validateStateId, StatesController.update);

routerState.delete('/states/:stateId', validateStateId, StatesController.delete);

export default routerState;

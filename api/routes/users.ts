import express, { Request, Response, NextFunction } from 'express';
import UsersController from '../controllers/UsersController';
import { User } from '../models/User';

const routerUser = express.Router();

const validateUserId = async (req: Request, res: Response, next: NextFunction) => {
  const user = await User.findByPk(req.params.userId);
  if (!user) {
    return res.status(404).json({ error: 'User not found' });
  }
  next();
}

routerUser.get('/users', UsersController.index);

routerUser.post('/users', UsersController.create);

routerUser.get('/users/:userId', validateUserId, UsersController.show);

routerUser.put('/users/:userId', validateUserId, UsersController.update);

routerUser.delete('/users/:userId', validateUserId, UsersController.delete);

export default routerUser;

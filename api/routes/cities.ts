import express, { Request, Response, NextFunction } from 'express';
import CitiesController from '../controllers/CitiesController';
import { City } from '../models/City';

const routerCity = express.Router();

const validateCityId = async (req: Request, res: Response, next: NextFunction) => {
    const city = await City.findByPk(req.params.cityId);
    if (!city) {
        return res.status(404).json({ error: 'City not found' });
    }
    next();
}

routerCity.post('/cities', CitiesController.create);

routerCity.get('/cities', CitiesController.index);

routerCity.get('/cities/:cityId', validateCityId, CitiesController.show);

routerCity.put('/cities/:cityId', validateCityId, CitiesController.update);

routerCity.delete('/cities/:cityId', validateCityId, CitiesController.delete);

export default routerCity;

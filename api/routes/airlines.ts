import express, { Request, Response, NextFunction } from 'express';
import AirlinesController from '../controllers/AirlinesController';
import { Airline } from '../models/Airline';

const routerAirline = express.Router();

const validateAirlineId = async (req: Request, res: Response, next: NextFunction) => {
    const city = await Airline.findByPk(req.params.airlineId);
    if (!city) {
        return res.status(404).json({ error: 'Airline not found' });
    }
    next();
}

routerAirline.post('/airlines', AirlinesController.create);

routerAirline.get('/airlines', AirlinesController.index);

routerAirline.get('/airlines/:airlineId', validateAirlineId, AirlinesController.show);

routerAirline.put('/airlines/:airlineId', validateAirlineId, AirlinesController.update);

routerAirline.delete('/airlines/:airlineId', validateAirlineId, AirlinesController.delete);

export default routerAirline;